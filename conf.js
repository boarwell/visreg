const PREV = 'prev.png';
const CURRENT = 'current.png';
const URL = 'http://localhost:8080/';

const DIFF_CONF = {
  reference: PREV,
  current: CURRENT,
  diff: 'diff.png',
  highlightColor: '#ff00ff',
  tolerance: 3
};

module.exports = {
  URL,
  PREV,
  CURRENT,
  DIFF_CONF
};
