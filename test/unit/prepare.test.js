// @ts-check

const conf = require('../../conf.js');
const fs = require('fs');
const prepare = require('../../lib/prepare.js');

describe('prepare.js', () => {
  describe('prepare()', () => {
    if (!fs.existsSync(conf.CURRENT)) {
      fs.closeSync(fs.openSync(`./${conf.CURRENT}`, 'w'));
    }

    if (fs.existsSync(conf.PREV)) {
      fs.unlinkSync(conf.PREV);
    }

    test('main', () => {
      prepare();
      expect(fs.existsSync(conf.CURRENT)).toBe(false);
      expect(fs.existsSync(conf.PREV)).toBe(true);
      // もし上のアサーションでコケたらこの行は実行されないようなので
      // 削除の前にexistsはしていない
      fs.unlinkSync(conf.PREV);
    });
  });
});
